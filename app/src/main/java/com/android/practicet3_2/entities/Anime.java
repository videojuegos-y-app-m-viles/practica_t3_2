package com.android.practicet3_2.entities;

public class Anime {

    private int id;
    private String name;
    private String information;
    private String image;
    private String favorito;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return this.information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFavorito() {
        return this.favorito;
    }

    public void setFavorito(String favorito) {
        this.favorito = favorito;
    }
}
