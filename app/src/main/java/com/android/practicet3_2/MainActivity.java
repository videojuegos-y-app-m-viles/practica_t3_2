package com.android.practicet3_2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.android.practicet3_2.adapters.AnimeAdapter;
import com.android.practicet3_2.entities.Anime;
import com.android.practicet3_2.services.IAnimeServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rcvAnimes = findViewById(R.id.rcvAnimes);
        rcvAnimes.setHasFixedSize(true);
        rcvAnimes.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://vdj-t3-2.free.beeceptor.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IAnimeServices services = retrofit.create(IAnimeServices.class);
        services.allAnimes().enqueue(new Callback<List<Anime>>() {
            @Override
            public void onResponse(Call<List<Anime>> call, Response<List<Anime>> response) {
                AnimeAdapter adapter = new AnimeAdapter(response.body());
                rcvAnimes.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Anime>> call, Throwable t) {

            }
        });

    }
}