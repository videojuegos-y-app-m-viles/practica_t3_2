package com.android.practicet3_2.adapters;

import android.graphics.drawable.Icon;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.practicet3_2.R;
import com.android.practicet3_2.entities.Anime;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AnimeAdapter extends RecyclerView.Adapter<AnimeAdapter.ViewHolder> {

private List<Anime> data;
private View view;

public AnimeAdapter(List<Anime> data) {
        this.data = data;
        }

@NonNull
@Override
public AnimeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_anime, parent, false);
        return new AnimeAdapter.ViewHolder(view);
        }

@Override
public void onBindViewHolder(ViewHolder holder, int position) {
        TextView tvName = holder.itemView.findViewById(R.id.tvName);
        TextView tvInformation = holder.itemView.findViewById(R.id.tvInformation);
        ImageView ivIconAnime = holder.itemView.findViewById(R.id.imageIcon);
        ImageView ivIconFavorite = holder.itemView.findViewById(R.id.imageFavorito);


        Anime anime = data.get(position);
        tvName.setText(anime.getName());
        tvInformation.setText(anime.getInformation());
        if(anime.getFavorito()=="Favorito")
             ivIconFavorite.setImageResource(R.mipmap.favorite);
        else ivIconFavorite.setImageResource(R.mipmap.nofavorite);
        holder.itemView.findViewById(R.id.imageFavorito).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        if(anime.getFavorito()=="Favorito")
                        {
                                Toast.makeText(view.getContext(), "Se retiró de tus favoritos", Toast.LENGTH_LONG).show();
                                anime.setFavorito("noFavorite");
                                ivIconFavorite.setImageResource(R.mipmap.nofavorite);
                        }
                        else {
                                anime.setFavorito("Favorito");
                                Toast.makeText(view.getContext(), "Se añadió a tus favoritos", Toast.LENGTH_LONG).show();
                                ivIconFavorite.setImageResource(R.mipmap.favorite);
                        }
                }
        });


        Picasso.get().load(anime.getImage()).into(ivIconAnime);

}

@Override
public int getItemCount() {
        return this.data.size();
        }

class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
                super(itemView);
        }
}

}