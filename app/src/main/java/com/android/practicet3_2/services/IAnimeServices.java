package com.android.practicet3_2.services;

import com.android.practicet3_2.entities.Anime;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IAnimeServices {

    @GET("animes")
    Call<List<Anime>> allAnimes();


}
